"""
Example ASGI config for licenses application.

https://docs.djangoproject.com/en/3.0/howto/deployment/asgi/
"""

import os

from django.core.asgi import get_asgi_application

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "examples.settings")

application = get_asgi_application()
