"""Test URL routing of licenses app."""

from typing import List, Union

from django.urls import URLPattern, URLResolver, include, path

urlpatterns: List[Union[URLPattern, URLResolver]] = [
    path("", include("licenses.urls")),
]
