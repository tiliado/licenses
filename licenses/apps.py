"""Licenses app configuration."""
from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class LicensesConfig(AppConfig):
    """Licenses app configuration."""

    name = "licenses"
    verbose_name = _("Licenses")
