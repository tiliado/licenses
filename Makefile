APP = licenses
PYTHON ?= python3.8
VENV ?= ${HOME}/.venv/${APP}
TOOLS_VENV ?= ${HOME}/.venv/${APP}-tools
TRANSLATIONS = ${APP}/locale/cs/LC_MESSAGES/django.po

.PHONY: default msg msg-py msg-make-py msg-sort-py test isort black clean venv devel-env clean-devel-env

default: test

# Translations
msg: msg-py

msg-py: msg-make-py msg-sort-py

msg-make-py:
	unset -v DJANGO_SETTINGS_MODULE; cd ${APP} && django-admin makemessages -l cs

msg-sort-py:
	msgattrib --sort-output --no-location --no-obsolete -o ${TRANSLATIONS} ${TRANSLATIONS}

test: ${TOOLS_VENV}/bin/tox
	${TOOLS_VENV}/bin/tox --parallel all --parallel-live

isort: ${TOOLS_VENV}/bin/isort
	${TOOLS_VENV}/bin/isort --recursive ${APP} examples

black: ${TOOLS_VENV}/bin/black
	${TOOLS_VENV}/bin/black setup.py ${APP} examples

${VENV}/bin/python:
	mkdir -p "$(dir ${VENV})"
	${PYTHON} -m venv "${VENV}"

${TOOLS_VENV}/bin/python:
	mkdir -p "$(dir $TOOLS_VENV)"
	${PYTHON} -m venv "${TOOLS_VENV}"
	${TOOLS_VENV}/bin/pip install -U pip setuptools wheel

${TOOLS_VENV}/bin/isort: ${TOOLS_VENV}/bin/python
	${TOOLS_VENV}/bin/pip install -U isort

${TOOLS_VENV}/bin/black: ${TOOLS_VENV}/bin/python
	${TOOLS_VENV}/bin/pip install -U black

${TOOLS_VENV}/bin/tox: ${TOOLS_VENV}/bin/python
	${TOOLS_VENV}/bin/pip install -U tox

venv: ${VENV}/bin/python
	${VENV}/bin/pip install -U pip setuptools wheel
	${VENV}/bin/pip install -U -c constraints.txt -r requirements.txt

examples/bash_aliases.sh:
	echo "alias app_${APP}='${VENV}/bin/python ${CURDIR}/examples/manage.py'" >> examples/bash_aliases.sh
	@echo "Source '${CURDIR}/examples/bash_aliases.sh' in your ~/.bashrc file."

devel-env: venv ${TOOLS_VENV}/bin/black ${TOOLS_VENV}/bin/isort ${TOOLS_VENV}/bin/tox examples/bash_aliases.sh

clean:
	rm -rf .tox .mypy_cache .coverage* htmlcov build *.egg-info $(TRANSLATIONS:.po=.mo)
	find . -name __pycache__ -type d -exec rm -r {} +

clean-devel-env:
	rm -rf ${VENV} ${TOOLS_VENV} examples/bash_aliases.sh
