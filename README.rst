Licenses
========

Application to manage extra license keys for Nuvola_. |Pipeline|_ |Coverage|_

Local Development
------------------

Default Makefile variables may be overridden with ``FOO=foo make ...``:

* ``PYTHON=python3.8`` -- base Python interpreter
* ``VENV=$HOME/.venv/licenses`` -- Python virtual environment for the app.
* ``TOOLS_VENV=$HOME/.venv/licenses-tools`` -- Python virtual environment for various tools.

Create development environment with ``make devel-env``.

* It creates virtual environments at ``$VENV`` and ``$TOOLS_VENV``.
* It installs app requirements according to ``constraints.txt`` and ``requirements.txt`` into ``$VENV``.
* It installs various tools into ``$TOOLS_VENV``.
* It provides a few Bash aliases in a new file ``examples/bash_aliases.sh``.


.. _Nuvola: https://nuvola.tiliado.eu

.. |Pipeline| image:: https://gitlab.com/tiliado/licenses/badges/master/pipeline.svg
.. _Pipeline: https://gitlab.com/tiliado/licenses/pipelines?ref=master

.. |Coverage| image:: https://gitlab.com/tiliado/licenses/badges/master/coverage.svg
.. _Coverage: https://gitlab.com/tiliado/licenses/pipelines?ref=master
