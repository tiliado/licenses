#!/usr/bin/env python3

from setuptools import find_packages, setup

PROJECT_NAME = "licenses"
INSTALL_REQUIRES = open("requirements.txt").read().splitlines()
EXTRAS_REQUIRE = {
    "quality": ["isort", "flake8", "pydocstyle", "polint", "mypy", "bandit", "black"],
    "test": [],  # placeholder
}


def main():
    setup(
        name=PROJECT_NAME,
        description="Tiliado licenses app",
        author="Jiří Janoušek",
        author_email="janousek.jiri@gmail.com",
        platforms=["posix"],
        python_requires=">=3.8",
        install_requires=INSTALL_REQUIRES,
        extras_require=EXTRAS_REQUIRE,
        packages=find_packages(exclude=["examples"]),
        include_package_data=True,
    )


if __name__ == "__main__":
    main()
